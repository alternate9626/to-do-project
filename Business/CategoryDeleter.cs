﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLibrary
{
    public class CategoryDeleter
    {
        CategoryListSaver CategoryListSaver;        
        CategoryListReaderFromFile CategoryListReader;

        public CategoryDeleter(String CategorySaveFilePath)
        {
            CategoryListSaver = new CategoryListSaver(CategorySaveFilePath);
            CategoryListReader = new CategoryListReaderFromFile(CategorySaveFilePath);
        }

        public void DeleteCategory(Category category)
        {
            List<Category> listOfCategories = CategoryListReader.ReadListOfCategories();
            listOfCategories.RemoveAt(category.ID);
            CategoryListSaver.Save(listOfCategories);
        }  
    }
}
