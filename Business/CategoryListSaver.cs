﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace BusinessLogicLibrary
{
    public class CategoryListSaver
    {
        String CategorySaveFilePath;

        public CategoryListSaver(String CategorySaveFilePath)
        {
            this.CategorySaveFilePath = CategorySaveFilePath;
        }       

        public void Save(List<Category> updatedListOfCategories)
        {
            var serializer = new JavaScriptSerializer();
            var json = serializer.Serialize(this.UpdateListOfCategoriesIDs(updatedListOfCategories));
            File.WriteAllText(CategorySaveFilePath, json);
        }

        public List<Category> UpdateListOfCategoriesIDs(List<Category> listOfCategories)
        {
            for (int counter = 0; counter < listOfCategories.Count; counter++)
            {
                listOfCategories[counter].ID = counter;                
            }
            return listOfCategories;
        }  
    }
}
