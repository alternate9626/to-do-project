﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLibrary
{
    public class CategoryUpdater
    {
        CategoryListSaver CategoryListSaver;
        CategoryListReaderFromFile CategoryListReader;

        public CategoryUpdater(String CategorySaveFilePath)
        {
            this.CategoryListReader = new CategoryListReaderFromFile(CategorySaveFilePath);
            this.CategoryListSaver = new CategoryListSaver(CategorySaveFilePath);
        }

        public void UpdateCategory(Category CategoryUpdated)
        {
            List<Category> listOfCategories = this.CategoryListReader.ReadListOfCategories();
            listOfCategories[CategoryUpdated.ID].Name = CategoryUpdated.Name;
            listOfCategories[CategoryUpdated.ID].listOfTodos = CategoryUpdated.listOfTodos;
            CategoryListSaver.Save(listOfCategories);
        }
    }
}
