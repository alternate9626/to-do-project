﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLibrary
{
    public class Category
    {
        public String Name;
        public List<ToDo> listOfTodos;
        public int ID;
        
        public Category()
        {

        }

        public Category(String Name)
        {
            this.listOfTodos = new List<ToDo>();
            this.Name = Name;
           
        }

        public void CreateToDo(String name)
        {            
            ToDo toDo = new ToDo(name);
            toDo.ID = listOfTodos.Count;
            listOfTodos.Add(toDo);           
        }

        public void DeleteToDo(ToDo toDo)
        {            
            listOfTodos.RemoveAt(toDo.ID);
            UpdateToDosIDs();
        }

        public void UpdateToDo(ToDo toDoUpdated)
        {            
            listOfTodos[toDoUpdated.ID].Name = toDoUpdated.Name;
            listOfTodos[toDoUpdated.ID].State = toDoUpdated.State;
            UpdateToDosIDs();
        }

        private void UpdateToDosIDs()
        {
            for (int counter = 0; counter < listOfTodos.Count; counter++)
            {
                listOfTodos[counter].ID = counter;
            }
        }
    }
}
