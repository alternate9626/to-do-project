﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLibrary
{
    public class CategoryCreator
    {
        CategoryListSaver CategoryListSaver;
        CategoryListReaderFromFile CategoryListReader;

        public CategoryCreator(String CategorySaveFilePath)
        {
            this.CategoryListSaver = new CategoryListSaver(CategorySaveFilePath);
            this.CategoryListReader = new CategoryListReaderFromFile(CategorySaveFilePath);
        }        

        public void CreateCategory(String name)
        {
            List<Category> listOfCategories = CategoryListReader.ReadListOfCategories();
            Category category = new Category(name);
            category.ID = listOfCategories.Count;
            listOfCategories.Add(category);
            CategoryListSaver.Save(listOfCategories);
        }       
    }
}
