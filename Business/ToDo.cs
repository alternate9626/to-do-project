﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLogicLibrary
{
    public class ToDo
    {
        public String Name;
        public bool State;
        public int ID;

        public ToDo()
        {

        }

        public ToDo(String name)
        {
            this.Name = name;
            this.State = false;            
        }        
    }
}
