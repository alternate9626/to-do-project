﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace BusinessLogicLibrary
{
    public class CategoryListReaderFromFile
    {
        String CategorySaveFilePath;

        public CategoryListReaderFromFile(String CategorySaveFilePath)
        {
            this.CategorySaveFilePath = CategorySaveFilePath;
        }

        public List<Category> ReadListOfCategories()
        {
            List<Category> listOfCategories = new List<Category>();
            var serializer = new JavaScriptSerializer();
            var json = serializer.Deserialize<List<Category>>(File.ReadAllText(CategorySaveFilePath));
            listOfCategories = json;            
            return listOfCategories;
        }      
    }
}
