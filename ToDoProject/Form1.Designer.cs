﻿namespace ToDoProject
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AddToDoButton = new System.Windows.Forms.Button();
            this.UserInputToDo = new System.Windows.Forms.TextBox();
            this.DeleteToDoButton = new System.Windows.Forms.Button();
            this.ListOfToDosDisplayed = new System.Windows.Forms.CheckedListBox();
            this.UpdateToDoButton = new System.Windows.Forms.Button();
            this.TabsControl = new System.Windows.Forms.TabControl();
            this.ToDoTab = new System.Windows.Forms.TabPage();
            this.CategoryInstructionLabel = new System.Windows.Forms.Label();
            this.UserInputCategory = new System.Windows.Forms.TextBox();
            this.ListOfCategoriesDisplayed = new System.Windows.Forms.ListBox();
            this.AddCategoryButton = new System.Windows.Forms.Button();
            this.UpdateCategoryButton = new System.Windows.Forms.Button();
            this.DeleteCategoryButton = new System.Windows.Forms.Button();
            this.CategoryTab = new System.Windows.Forms.TabPage();
            this.CategoryNameLabel = new System.Windows.Forms.Label();
            this.UserInputCategoriesTab = new System.Windows.Forms.TextBox();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorker2 = new System.ComponentModel.BackgroundWorker();
            this.TabsControl.SuspendLayout();
            this.ToDoTab.SuspendLayout();
            this.CategoryTab.SuspendLayout();
            this.SuspendLayout();
            // 
            // AddToDoButton
            // 
            this.AddToDoButton.Location = new System.Drawing.Point(195, 45);
            this.AddToDoButton.Name = "AddToDoButton";
            this.AddToDoButton.Size = new System.Drawing.Size(95, 30);
            this.AddToDoButton.TabIndex = 0;
            this.AddToDoButton.Text = "Add To Do";
            this.AddToDoButton.UseVisualStyleBackColor = true;
            this.AddToDoButton.Click += new System.EventHandler(this.AddToDoButton_Click);
            // 
            // UserInputToDo
            // 
            this.UserInputToDo.Location = new System.Drawing.Point(47, 19);
            this.UserInputToDo.Name = "UserInputToDo";
            this.UserInputToDo.Size = new System.Drawing.Size(243, 20);
            this.UserInputToDo.TabIndex = 2;
            // 
            // DeleteToDoButton
            // 
            this.DeleteToDoButton.Location = new System.Drawing.Point(75, 268);
            this.DeleteToDoButton.Name = "DeleteToDoButton";
            this.DeleteToDoButton.Size = new System.Drawing.Size(185, 48);
            this.DeleteToDoButton.TabIndex = 5;
            this.DeleteToDoButton.Text = "Delete To Do";
            this.DeleteToDoButton.UseVisualStyleBackColor = true;
            this.DeleteToDoButton.Click += new System.EventHandler(this.DeleteToDoButton_Click);
            // 
            // ListOfToDosDisplayed
            // 
            this.ListOfToDosDisplayed.FormattingEnabled = true;
            this.ListOfToDosDisplayed.Location = new System.Drawing.Point(47, 93);
            this.ListOfToDosDisplayed.Name = "ListOfToDosDisplayed";
            this.ListOfToDosDisplayed.Size = new System.Drawing.Size(243, 169);
            this.ListOfToDosDisplayed.TabIndex = 7;
            this.ListOfToDosDisplayed.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.ListOfToDosDisplayed_ItemCheck);
            // 
            // UpdateToDoButton
            // 
            this.UpdateToDoButton.Location = new System.Drawing.Point(47, 45);
            this.UpdateToDoButton.Name = "UpdateToDoButton";
            this.UpdateToDoButton.Size = new System.Drawing.Size(103, 30);
            this.UpdateToDoButton.TabIndex = 9;
            this.UpdateToDoButton.Text = "Update Selected Item";
            this.UpdateToDoButton.UseVisualStyleBackColor = true;
            this.UpdateToDoButton.Click += new System.EventHandler(this.UpdateToDoButton_Click);
            // 
            // TabsControl
            // 
            this.TabsControl.Controls.Add(this.ToDoTab);
            this.TabsControl.Controls.Add(this.CategoryTab);
            this.TabsControl.Location = new System.Drawing.Point(16, 5);
            this.TabsControl.Name = "TabsControl";
            this.TabsControl.SelectedIndex = 0;
            this.TabsControl.Size = new System.Drawing.Size(747, 360);
            this.TabsControl.TabIndex = 10;
            this.TabsControl.Tag = "";
            // 
            // ToDoTab
            // 
            this.ToDoTab.Controls.Add(this.CategoryInstructionLabel);
            this.ToDoTab.Controls.Add(this.UserInputCategory);
            this.ToDoTab.Controls.Add(this.ListOfCategoriesDisplayed);
            this.ToDoTab.Controls.Add(this.AddCategoryButton);
            this.ToDoTab.Controls.Add(this.UpdateCategoryButton);
            this.ToDoTab.Controls.Add(this.DeleteCategoryButton);
            this.ToDoTab.Location = new System.Drawing.Point(4, 22);
            this.ToDoTab.Name = "ToDoTab";
            this.ToDoTab.Padding = new System.Windows.Forms.Padding(3);
            this.ToDoTab.Size = new System.Drawing.Size(739, 334);
            this.ToDoTab.TabIndex = 0;
            this.ToDoTab.Text = "Category";
            this.ToDoTab.UseVisualStyleBackColor = true;
            // 
            // CategoryInstructionLabel
            // 
            this.CategoryInstructionLabel.AutoSize = true;
            this.CategoryInstructionLabel.Location = new System.Drawing.Point(40, 39);
            this.CategoryInstructionLabel.Name = "CategoryInstructionLabel";
            this.CategoryInstructionLabel.Size = new System.Drawing.Size(206, 13);
            this.CategoryInstructionLabel.TabIndex = 16;
            this.CategoryInstructionLabel.Text = "Double click a category to access ToDos.";
            // 
            // UserInputCategory
            // 
            this.UserInputCategory.Location = new System.Drawing.Point(43, 16);
            this.UserInputCategory.Name = "UserInputCategory";
            this.UserInputCategory.Size = new System.Drawing.Size(243, 20);
            this.UserInputCategory.TabIndex = 15;
            // 
            // ListOfCategoriesDisplayed
            // 
            this.ListOfCategoriesDisplayed.FormattingEnabled = true;
            this.ListOfCategoriesDisplayed.Location = new System.Drawing.Point(43, 88);
            this.ListOfCategoriesDisplayed.Name = "ListOfCategoriesDisplayed";
            this.ListOfCategoriesDisplayed.Size = new System.Drawing.Size(243, 173);
            this.ListOfCategoriesDisplayed.TabIndex = 14;
            this.ListOfCategoriesDisplayed.SelectedIndexChanged += new System.EventHandler(this.ListOfCategoriesDisplayed_SelectedIndexChanged);
            this.ListOfCategoriesDisplayed.DoubleClick += new System.EventHandler(this.ListOfCategoriesDisplayed_DoubleClick);
            // 
            // AddCategoryButton
            // 
            this.AddCategoryButton.Location = new System.Drawing.Point(191, 52);
            this.AddCategoryButton.Name = "AddCategoryButton";
            this.AddCategoryButton.Size = new System.Drawing.Size(95, 30);
            this.AddCategoryButton.TabIndex = 10;
            this.AddCategoryButton.Text = "Add Category";
            this.AddCategoryButton.UseVisualStyleBackColor = true;
            this.AddCategoryButton.Click += new System.EventHandler(this.AddCategoryButton_Click);
            // 
            // UpdateCategoryButton
            // 
            this.UpdateCategoryButton.Location = new System.Drawing.Point(43, 52);
            this.UpdateCategoryButton.Name = "UpdateCategoryButton";
            this.UpdateCategoryButton.Size = new System.Drawing.Size(103, 30);
            this.UpdateCategoryButton.TabIndex = 13;
            this.UpdateCategoryButton.Text = "Update";
            this.UpdateCategoryButton.UseVisualStyleBackColor = true;
            this.UpdateCategoryButton.Click += new System.EventHandler(this.UpdateCategoryButton_Click);
            // 
            // DeleteCategoryButton
            // 
            this.DeleteCategoryButton.Location = new System.Drawing.Point(73, 268);
            this.DeleteCategoryButton.Name = "DeleteCategoryButton";
            this.DeleteCategoryButton.Size = new System.Drawing.Size(185, 48);
            this.DeleteCategoryButton.TabIndex = 11;
            this.DeleteCategoryButton.Text = "Delete Category";
            this.DeleteCategoryButton.UseVisualStyleBackColor = true;
            this.DeleteCategoryButton.Click += new System.EventHandler(this.DeleteCategoryButton_Click);
            // 
            // CategoryTab
            // 
            this.CategoryTab.Controls.Add(this.CategoryNameLabel);
            this.CategoryTab.Controls.Add(this.ListOfToDosDisplayed);
            this.CategoryTab.Controls.Add(this.UserInputToDo);
            this.CategoryTab.Controls.Add(this.AddToDoButton);
            this.CategoryTab.Controls.Add(this.DeleteToDoButton);
            this.CategoryTab.Controls.Add(this.UpdateToDoButton);
            this.CategoryTab.Location = new System.Drawing.Point(4, 22);
            this.CategoryTab.Name = "CategoryTab";
            this.CategoryTab.Padding = new System.Windows.Forms.Padding(3);
            this.CategoryTab.Size = new System.Drawing.Size(739, 334);
            this.CategoryTab.TabIndex = 1;
            this.CategoryTab.Text = "To Do\'s";
            this.CategoryTab.UseVisualStyleBackColor = true;
            // 
            // CategoryNameLabel
            // 
            this.CategoryNameLabel.AutoSize = true;
            this.CategoryNameLabel.Location = new System.Drawing.Point(44, 78);
            this.CategoryNameLabel.Name = "CategoryNameLabel";
            this.CategoryNameLabel.Size = new System.Drawing.Size(130, 13);
            this.CategoryNameLabel.TabIndex = 10;
            this.CategoryNameLabel.Text = "ToDos inside Category \" \"";
            this.CategoryNameLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // UserInputCategoriesTab
            // 
            this.UserInputCategoriesTab.Location = new System.Drawing.Point(368, -28);
            this.UserInputCategoriesTab.Name = "UserInputCategoriesTab";
            this.UserInputCategoriesTab.Size = new System.Drawing.Size(243, 20);
            this.UserInputCategoriesTab.TabIndex = 3;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(804, 376);
            this.Controls.Add(this.TabsControl);
            this.Controls.Add(this.UserInputCategoriesTab);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "To Do Program";
            this.TabsControl.ResumeLayout(false);
            this.ToDoTab.ResumeLayout(false);
            this.ToDoTab.PerformLayout();
            this.CategoryTab.ResumeLayout(false);
            this.CategoryTab.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button AddToDoButton;
        private System.Windows.Forms.TextBox UserInputToDo;
        private System.Windows.Forms.Button DeleteToDoButton;
        private System.Windows.Forms.CheckedListBox ListOfToDosDisplayed;
        private System.Windows.Forms.Button UpdateToDoButton;
        private System.Windows.Forms.TabControl TabsControl;
        private System.Windows.Forms.TabPage ToDoTab;
        private System.Windows.Forms.TabPage CategoryTab;
        private System.Windows.Forms.TextBox UserInputCategoriesTab;
        private System.Windows.Forms.Button DeleteCategoryButton;
        private System.Windows.Forms.Button UpdateCategoryButton;
        private System.Windows.Forms.Button AddCategoryButton;
        private System.Windows.Forms.ListBox ListOfCategoriesDisplayed;
        private System.Windows.Forms.Label CategoryNameLabel;
        private System.Windows.Forms.TextBox UserInputCategory;
        private System.Windows.Forms.Label CategoryInstructionLabel;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.ComponentModel.BackgroundWorker backgroundWorker2;
       
        
        
    }
}

