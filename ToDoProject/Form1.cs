﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using BusinessLogicLibrary;
using System.Web.Script.Serialization;


namespace ToDoProject
{
    public partial class Form1 : Form
    {     
        List<Category> listOfCategories;
        CategoryCreator categoryCreator;
        CategoryListReaderFromFile categoryListReader;
        CategoryUpdater categoryUpdater;
        CategoryDeleter categoryDeleter;
        CategoryListSaver categoryListSaver;

        public Form1()
        {
            InitializeComponent();
            if (!File.Exists("Save.txt"))
            {
                File.WriteAllText(@"Save.txt", "[]");
            }
            String categorySaveFilePath = @"Save.txt";
            
            categoryCreator = new CategoryCreator(categorySaveFilePath);
            categoryUpdater = new CategoryUpdater(categorySaveFilePath);
            categoryDeleter = new CategoryDeleter(categorySaveFilePath);
            categoryListReader = new CategoryListReaderFromFile(categorySaveFilePath);
            categoryListSaver = new CategoryListSaver(categorySaveFilePath);
            listOfCategories = categoryListReader.ReadListOfCategories();
            RefreshListOfCategoriesDisplayed(); 
            RefreshListOfToDosDisplayed();            
        }

        private void RefreshListOfToDosDisplayed()
        {
            if (IsThereACategorySelected())
            {
                ListOfToDosDisplayed.Items.Clear();                
                for (int counter = 0; counter < listOfCategories[ListOfCategoriesDisplayed.SelectedIndex].listOfTodos.Count; counter++)
                {
                    ListOfToDosDisplayed.Items.Add(listOfCategories[ListOfCategoriesDisplayed.SelectedIndex].listOfTodos[counter].Name, listOfCategories[ListOfCategoriesDisplayed.SelectedIndex].listOfTodos[counter].State);
                }
                
                CategoryNameLabel.Text = "ToDo's displayed for '" + listOfCategories[ListOfCategoriesDisplayed.SelectedIndex].Name + "' category.";
            }
            else
            {
                ListOfToDosDisplayed.Items.Clear();
                categoryListSaver.Save(listOfCategories);
                CategoryNameLabel.Text = "ToDo's displayed for ' ' category.";
            }
        }       

        private void RefreshListOfCategoriesDisplayed()
        {
            ListOfCategoriesDisplayed.Items.Clear();
            listOfCategories = categoryListReader.ReadListOfCategories();            
                for (int counter = 0; counter < listOfCategories.Count; counter++)
                {
                    ListOfCategoriesDisplayed.Items.Add(listOfCategories[counter].Name);                    
                }
                this.RefreshListOfToDosDisplayed();                
        }

        private void AddToDoButton_Click(object sender, EventArgs e)
        {            
            if (IsInputValid(UserInputToDo.Text) && IsThereACategorySelected())
            {        
                listOfCategories[ListOfCategoriesDisplayed.SelectedIndex].CreateToDo(UserInputToDo.Text);
                UserInputToDo.Clear();
                RefreshListOfToDosDisplayed();
            }
        }

        private void AddCategoryButton_Click(object sender, EventArgs e)
        {
            if (IsInputValid(UserInputCategory.Text))
            {
                categoryCreator.CreateCategory(UserInputCategory.Text);
                UserInputCategory.Clear();
                RefreshListOfCategoriesDisplayed();
            }
        } 

        public bool IsInputValid(String input)
        {
            return (input.Trim() != "");
        }  
      
        private void UpdateToDoButton_Click(object sender, EventArgs e)
        {
            if (IsInputValid(UserInputToDo.Text) && IsThereAToDoSelected() && IsThereACategorySelected())
            {                              
                listOfCategories[ListOfCategoriesDisplayed.SelectedIndex].listOfTodos[ListOfToDosDisplayed.SelectedIndex].Name = UserInputToDo.Text;
                listOfCategories[ListOfCategoriesDisplayed.SelectedIndex].UpdateToDo(listOfCategories[ListOfCategoriesDisplayed.SelectedIndex].listOfTodos[ListOfToDosDisplayed.SelectedIndex]);
                UserInputToDo.Clear();
            }   
             RefreshListOfToDosDisplayed();            
        }

        private void UpdateCategoryButton_Click(object sender, EventArgs e)
        {
            if (IsInputValid(UserInputCategory.Text) && IsThereACategorySelected())
            {
                int indexOfCategorySelected = ListOfCategoriesDisplayed.SelectedIndex;
                listOfCategories[ListOfCategoriesDisplayed.SelectedIndex].Name = UserInputCategory.Text;
                categoryUpdater.UpdateCategory(listOfCategories[ListOfCategoriesDisplayed.SelectedIndex]);
                UserInputCategory.Clear();
                RefreshListOfCategoriesDisplayed();
                ListOfCategoriesDisplayed.SelectedIndex = indexOfCategorySelected;
                RefreshListOfToDosDisplayed();
            }                      
        }

        public bool IsThereAToDoSelected()
        {
            if (ListOfToDosDisplayed.SelectedIndex >= 0)
            {
                return (ListOfToDosDisplayed.SelectedIndex < listOfCategories[ListOfCategoriesDisplayed.SelectedIndex].listOfTodos.Count) && (ListOfToDosDisplayed.SelectedIndex >= 0);
            }
            return false;
        }

        public bool IsThereACategorySelected()
        {
            return (ListOfCategoriesDisplayed.SelectedIndex < listOfCategories.Count) && (ListOfCategoriesDisplayed.SelectedIndex >= 0);
        }

        private void DeleteToDoButton_Click(object sender, EventArgs e)
        {
            if (IsThereAToDoSelected() && IsThereACategorySelected())
            {
                listOfCategories[ListOfCategoriesDisplayed.SelectedIndex].DeleteToDo(listOfCategories[ListOfCategoriesDisplayed.SelectedIndex].listOfTodos[ListOfToDosDisplayed.SelectedIndex]);
            }           
            RefreshListOfToDosDisplayed();
        }

        private void DeleteCategoryButton_Click(object sender, EventArgs e)
        {
            if (IsThereACategorySelected())
            {
                categoryDeleter.DeleteCategory(listOfCategories[ListOfCategoriesDisplayed.SelectedIndex]);
            }
            RefreshListOfCategoriesDisplayed();
        }

        private void ListOfToDosDisplayed_ItemCheck(object sender, ItemCheckEventArgs e)
        {
            if (IsThereAToDoSelected())
            {       
                listOfCategories[ListOfCategoriesDisplayed.SelectedIndex].listOfTodos[ListOfToDosDisplayed.SelectedIndex].State = !listOfCategories[ListOfCategoriesDisplayed.SelectedIndex].listOfTodos[ListOfToDosDisplayed.SelectedIndex].State;
                listOfCategories[ListOfCategoriesDisplayed.SelectedIndex].UpdateToDo(listOfCategories[ListOfCategoriesDisplayed.SelectedIndex].listOfTodos[ListOfToDosDisplayed.SelectedIndex]);
                RefreshListOfToDosDisplayed();
            }
        }        

        private void ListOfCategoriesDisplayed_DoubleClick(object sender, EventArgs e)
        {
            TabsControl.SelectedTab = CategoryTab;
            this.RefreshListOfToDosDisplayed();
        }

        private void ListOfCategoriesDisplayed_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.RefreshListOfToDosDisplayed();
        }

    }
}
