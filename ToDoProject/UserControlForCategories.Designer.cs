﻿namespace ToDoProject
{
    partial class UserControlForCategories
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.UserInputCategory = new System.Windows.Forms.TextBox();
            this.ListOfCategoriesDisplayed = new System.Windows.Forms.ListBox();
            this.AddCategoryButton = new System.Windows.Forms.Button();
            this.UpdateCategoryButton = new System.Windows.Forms.Button();
            this.DeleteCategoryButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // UserInputCategory
            // 
            this.UserInputCategory.Location = new System.Drawing.Point(51, 51);
            this.UserInputCategory.Name = "UserInputCategory";
            this.UserInputCategory.Size = new System.Drawing.Size(243, 20);
            this.UserInputCategory.TabIndex = 20;
            // 
            // ListOfCategoriesDisplayed
            // 
            this.ListOfCategoriesDisplayed.FormattingEnabled = true;
            this.ListOfCategoriesDisplayed.Location = new System.Drawing.Point(51, 123);
            this.ListOfCategoriesDisplayed.Name = "ListOfCategoriesDisplayed";
            this.ListOfCategoriesDisplayed.Size = new System.Drawing.Size(243, 173);
            this.ListOfCategoriesDisplayed.TabIndex = 19;
  //          this.ListOfCategoriesDisplayed.SelectedIndexChanged += new System.EventHandler(this.ListOfCategoriesDisplayed_SelectedIndexChanged);
  //          this.ListOfCategoriesDisplayed.DoubleClick += new System.EventHandler(this.ListOfCategoriesDisplayed_DoubleClick);
            // 
            // AddCategoryButton
            // 
            this.AddCategoryButton.Location = new System.Drawing.Point(199, 87);
            this.AddCategoryButton.Name = "AddCategoryButton";
            this.AddCategoryButton.Size = new System.Drawing.Size(95, 30);
            this.AddCategoryButton.TabIndex = 16;
            this.AddCategoryButton.Text = "Add Category";
            this.AddCategoryButton.UseVisualStyleBackColor = true;
  //          this.AddCategoryButton.Click += new System.EventHandler(this.AddCategoryButton_Click);
            // 
            // UpdateCategoryButton
            // 
            this.UpdateCategoryButton.Location = new System.Drawing.Point(51, 87);
            this.UpdateCategoryButton.Name = "UpdateCategoryButton";
            this.UpdateCategoryButton.Size = new System.Drawing.Size(103, 30);
            this.UpdateCategoryButton.TabIndex = 18;
            this.UpdateCategoryButton.Text = "Update";
            this.UpdateCategoryButton.UseVisualStyleBackColor = true;
  //          this.UpdateCategoryButton.Click += new System.EventHandler(this.UpdateCategoryButton_Click);
            // 
            // DeleteCategoryButton
            // 
            this.DeleteCategoryButton.Location = new System.Drawing.Point(81, 303);
            this.DeleteCategoryButton.Name = "DeleteCategoryButton";
            this.DeleteCategoryButton.Size = new System.Drawing.Size(185, 48);
            this.DeleteCategoryButton.TabIndex = 17;
            this.DeleteCategoryButton.Text = "Delete Category";
            this.DeleteCategoryButton.UseVisualStyleBackColor = true;
    //        this.DeleteCategoryButton.Click += new System.EventHandler(this.DeleteCategoryButton_Click);
            // 
            // UserControlForCategories
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.UserInputCategory);
            this.Controls.Add(this.ListOfCategoriesDisplayed);
            this.Controls.Add(this.AddCategoryButton);
            this.Controls.Add(this.UpdateCategoryButton);
            this.Controls.Add(this.DeleteCategoryButton);
            this.Name = "UserControlForCategories";
            this.Size = new System.Drawing.Size(344, 403);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox UserInputCategory;
        private System.Windows.Forms.ListBox ListOfCategoriesDisplayed;
        private System.Windows.Forms.Button AddCategoryButton;
        private System.Windows.Forms.Button UpdateCategoryButton;
        private System.Windows.Forms.Button DeleteCategoryButton;
    }
}
