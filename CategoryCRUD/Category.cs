﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CategoryCRUD
{
    public class Category
    {
        String Name;
        String Color;

        public Category(String name)
        {
            this.Name = name;
            this.Color = "Default";
        }
    }
}
